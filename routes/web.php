<?php

// NicolasModulo.com  = rout::get (/,funcion)
// NicolasModulo.com/contacto  = rout::get (contacto,funcion)
// las routas van con / 
//------------Pruebas-------------------
Route::get('/mauricio', function () {
    return 'Hola Caracola';
}) ->name('mau');

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/saludo/{nombre}', function ($nombre) {
    return 'wuena wuena...'. $nombre;
});
//------------nombres routas-------------------
Route::get('/contacto', function () {
    return 'Hola Caracola';
}) ->name('contacto');

Route::get('inicio', function () {
    echo"<a href='".route('contacto')."'>-contacto 1     </a> <br>";
    echo"<a href='".route('mau')."'>-saludos2            </a> <br>";
    echo"<a href='".route('home')."'>-contacto 3         </a> <br>";
    echo"<a href='".route('sename')."'>-AdminLTE_uno     </a> <br>";
    echo"<a href='".route('calendar')."'>-AdminLTE_dos   </a> <br>";
    echo"<a href='".route('login')."'>  -login          </a> <br>";
    
   });
//-------------------------------------
//------------VISTAS-------------------
Route::get('home', function () {
    return  view('home');
}) ->name('home');
//---------------------------------------
//------------AdminLTE-------------------
Route::get('admin', function () {
    return view('admin.dashboard');
})->name('sename');   

Route::get('calendar', function () {
    return  view('calendar');
}) ->name('calendar');

Route::get('index', function () {  
    return  view('index');
}) ->name('index');

//------------Modulos-------------------
Route::get('ModuloPrueba', function () {
    return  view('index');
}) ->name('ModuloPrueba');

Route::get('FormularoModulo', function () {
    return  view('index');
}) ->name('FormularoModulo');
//------------login-------------------

Route::post('login', 'Auth\LoginController@login')
->name('login');

Route::get('/login2', function () {  
    return  view('Auth\login2');
}) ->name('login2');

Route::get('/logo2', function () {  
    return  view('Auth\logo2');
}) ->name('logo2');

Route::get('/log', function () {  
    return  view('Auth\log');
}) ->name('log');

//------------cotizacion-------------------

Route::get('/cotizacion', function () {  
    return  view('cotizacion\cotizacion');
}) ->name('cotizacion');

//------------cotizacion2-------------------

Route::get('/carritoBKNS', function () {  
    return  view('cotizacion\carritoBKNS');
}) ->name('carritoBKNS');

Route::get('/galeria', function () {  
    return  view('cotizacion\galeria');
}) ->name('galeria');

Route::get('/cotizacion2', function () {  
    return  view('cotizacion\cotizacion2');
}) ->name('cotizacion2');



Route::get('/contacto', 'MensajesController@create');
//------------formulario-------------------
  Route::get('/formulario', function () {  
   return  view('cotizacion/formulario');
 }) ->name('formulario');



 





